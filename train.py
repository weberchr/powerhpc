#!/usr/bin/env python
import numpy
import math
import pickle
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import tensorflow as tf
keras = tf.keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from utilities import *

import matplotlib
matplotlib.use('agg')

# reading excel data
dfdir = 'data/Training2016.xlsx'
df = pd.read_excel(dfdir, header=0)
print(len)
df['timestamp'] = pd.to_datetime(df['timestamp'], format='%Y-%m-%d %H:%M:%S')


# reduce number of features in the data frame
df = reduce_features(df)

# preprocess the dataframe
# dataframe for 5min prediction
X5, y5 = preprocess_dataset(df, steps=1)
# dataframe for 60 min prediction (12 steps)
X60, y60 = preprocess_dataset(df, steps=12)

#sc = MinMaxScaler(feature_range = (0,1))
#X = sc.fit_transform(X)

#create small test set for 1h plot
X1h, y1h = get_1h_window(df)

# create 2 models
# 1st model for next 5 min p_ww prediction
# 2nd model for next 12 (60min) p_ww predictions
modellist = []
for xys in [(X5, y5, 1), (X60, y60, 12)]:
    X, y, timesteps = xys

    # Daten in Test- und Trainingsdaten aufteilen
    splitpoint = int(len(X)*0.2)
    X_test = X[:splitpoint]
    y_test = y[:splitpoint]
    X_train = X[splitpoint:]
    y_train = y[splitpoint:]

    def mlp_model(input_dim):
        # create and fit Multilayer Perceptron model (MLP)
        model = Sequential()
        model.add(Dense(24, input_dim=input_dim, activation='relu'))
        model.add(Dense(18, input_dim=input_dim, activation='relu'))
        model.add(Dense(12, input_dim=input_dim, activation='relu'))
        model.add(Dense(timesteps))
        return model

    model = mlp_model(input_dim=7)
    model.summary()

    opt = keras.optimizers.Adam(lr=1e-5)
    lr_schedule = keras.callbacks.LearningRateScheduler(lambda epoch: 1e-5 * 10**(epoch / 20))
    model.compile(optimizer = opt, loss = 'mse', metrics=['mae','mape'])
    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='loss',mode='min', patience=20)
    modelname = 'best_model_' + str(timesteps) + '.h5'
    mc = tf.keras.callbacks.ModelCheckpoint(modelname, monitor='loss', mode='min', verbose=0, save_best_only=True)
    history = model.fit(X_train, y_train, epochs=100, batch_size=16, callbacks=[mc, lr_schedule, early_stopping], validation_data=(X_test, y_test))

    # write history to file
    historypath = 'data/train_history' + str(timesteps)
    with open(historypath, "wb") as handler:
        pickle.dump(history.history, handler)

    # Estimate model performance
    trainScore = model.evaluate(X_train, y_train, verbose=0)
    print('Train Score: %.2f MSE (%.2f RMSE) - %.2f MAE - %.2f MAPE' % (trainScore[0], math.sqrt(trainScore[0]), trainScore[1], trainScore[2]))
    testScore = model.evaluate(X_test, y_test, verbose=0)
    print('Test Score: %.2f MSE (%.2f RMSE) - %.2f MAE - %.2f MAPE' % (testScore[0], math.sqrt(testScore[0]), testScore[1], testScore[2]))

    # save model
    modelname = 'mlp_model' + str(timesteps)
    model.save(modelname)
    modellist.append((model, history, X_test, y_test, timesteps))


history60 = modellist[1][1]
plot_loss(history60.history, 'data')

predict_and_plot(modellist[0][0], modellist[1][0], modellist[0][2], modellist[0][3], modellist[1][2],
                modellist[1][3], X1h, y1h, 'data')

print('models and plots saved')
