#!/usr/bin/env python
import numpy as np
import math
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.metrics as metrics

# preprocess dataframe
def preprocess_dataset(df, steps=1):
    X, y = [], []
    dflen = len(df)
    timescale = steps*5 #calculate time scale to 5 min steps

    # iterate over rows
    for i, currenttime in enumerate(df['timestamp']):

        # check if next steps are out of index
        if dflen <= i+steps:
            continue

        # get next time and calculate the time difference in minutes between the steps
        nexttime = df['timestamp'][i+steps]
        timediff = nexttime-currenttime
        timediff = timediff.total_seconds() / 60

        # check if currenttime minus lasttime equals around 5min

        if timescale-0.5 < timediff < timescale+0.5:
            y.append(np.asarray(df['P_WW'][i+1:i+1+steps], dtype=np.float32))       # append p_ww to y
            X.append(np.asarray(df.iloc[i][:-1], dtype=np.float32))    # append rest (from last entry) to X
    
    return np.array(X), np.array(y)


# get window for prediction plotting
def get_1h_window(df, steps=12):
    X, y = [], []
    dflen = len(df)
    timescale = (steps+1)*5 #calculate time scale to 5 min steps

    # iterate over rows
    for i, currenttime in enumerate(df['timestamp']):

        # check if next steps are out of index
        if dflen <= i+steps+1 or len(X)>0:
            break

        # get next time and calculate the time difference in minutes between the steps
        nexttime = df['timestamp'][i+steps+1]
        timediff = nexttime-currenttime
        timediff = timediff.total_seconds() / 60

        # check if currenttime minus lasttime equals around 5min

        if timescale-0.5 < timediff < timescale+0.5:
            y.append(np.asarray(df['P_WW'][i+1:i+1+steps], dtype=np.float32))       # append p_ww to y
            df = df.drop(['timestamp'], axis=1)
            X.append(np.asarray(df.iloc[i:i+steps], dtype=np.float32))    # append rest (from last entry) to X
            
    return np.array(X), np.array(y)


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def reduce_features(dataframe):

    # calculate means
    dataframe['avg_pump_speed'] = dataframe[['KLT11_pumpSpeed_p1','KLT11_pumpSpeed_p2', 'KLT11_pumpSpeed_p3', 'KLT12_pumpSpeed_p1','KLT12_pumpSpeed_p2', 'KLT12_pumpSpeed_p3', 'KLT13_pumpSpeed_p1','KLT13_pumpSpeed_p2', 'KLT13_pumpSpeed_p3', 'KLT14_pumpSpeed_p1','KLT14_pumpSpeed_p2', 'KLT14_pumpSpeed_p3']].mean(axis=1)
    dataframe['avg_flow_rate'] = dataframe[['KLT11_flowRate1', 'KLT11_flowRate2', 'KLT12_flowRate1', 'KLT12_flowRate2', 'KLT13_flowRate1', 'KLT14_flowRate2', 'KLT14_flowRate1', 'KLT14_flowRate2']].mean(axis=1)
    dataframe['avg_fan_speed'] = dataframe[['KLT11_Fan1Speed_HZ', 'KLT11_Fan2Speed_HZ', 'KLT12_Fan1Speed_HZ', 'KLT12_Fan2Speed_HZ', 'KLT13_Fan1Speed_HZ', 'KLT13_Fan2Speed_HZ', 'KLT14_Fan1Speed_HZ', 'KLT14_Fan2Speed_HZ']].mean(axis=1)
    dataframe['inlet_temp'] = dataframe[['KLT11_inletTempBeforeHydraulicGate', 'KLT12_inletTempBeforeHydraulicGate', 'KLT13_inletTempBeforeHydraulicGate', 'KLT14_inletTempBeforeHydraulicGate',]].mean(axis=1)
    dataframe['avg_bulb'] = dataframe[['dryBulb', 'wetBulb',]].mean(axis=1)

    dropme = ['dryBulb', 'wetBulb', 'relativeHumidity', 'P_SM_IT', 'KLT11_pumpSpeed_p1',
        'KLT11_pumpSpeed_p2', 'KLT11_pumpSpeed_p3', 'KLT12_pumpSpeed_p1',
        'KLT12_pumpSpeed_p2', 'KLT12_pumpSpeed_p3', 'KLT13_pumpSpeed_p1',
        'KLT13_pumpSpeed_p2', 'KLT13_pumpSpeed_p3', 'KLT14_pumpSpeed_p1',
        'KLT14_pumpSpeed_p2', 'KLT14_pumpSpeed_p3', 'KLT11_flowRate1',
        'KLT11_flowRate2', 'KLT12_flowRate1', 'KLT12_flowRate2',
        'KLT13_flowRate1', 'KLT13_flowRate2', 'KLT14_flowRate1',
        'KLT14_flowRate2', 'KLT11_Fan1Speed_HZ', 'KLT11_Fan2Speed_HZ',
        'KLT12_Fan1Speed_HZ', 'KLT12_Fan2Speed_HZ', 'KLT13_Fan1Speed_HZ',
        'KLT13_Fan2Speed_HZ', 'KLT14_Fan1Speed_HZ', 'KLT14_Fan2Speed_HZ',
        'KLT11_inletTempBeforeHydraulicGate',
        'KLT12_inletTempBeforeHydraulicGate',
        'KLT13_inletTempBeforeHydraulicGate',
        'KLT14_inletTempBeforeHydraulicGate',]

    # remove redundant columns
    for column in dropme:
        dataframe = dataframe.drop(column, axis=1)
    
    # move timestamp column to last position
    dataframe = dataframe[[c for c in dataframe if not c in ['timestamp']] + ['timestamp']]

    return dataframe


def predict_and_plot(model_5min, model_60min, X5, y5, X60, y60, X1h, y1h, path):

    # generate predictions for small 1h test set
    testPredict_1h_5min = model_5min.predict(X1h)
    testPredict_1h_60min = model_60min.predict(np.asarray([X1h[0][0]]))

    # plot baseline and predictions
    x = [i*5 for i in range(len(testPredict_1h_5min[0]))]
    plt.plot(x, y1h[0])
    plt.plot(x, testPredict_1h_5min.reshape(1, 12)[0])
    plt.plot(x, testPredict_1h_60min.reshape(1, 12)[0])
    plt.xticks(x)
    plt.xlabel('Time')
    plt.ylabel('P_WW')
    plt.legend(['Baseline', '5min Prediction', '1h Prediction'], loc=3)
    plt.savefig(path+'/predict_1h.png')
    plt.clf()

    # calculate metrics for next 5 min with 5min model
    pred_full_5minmodell = model_5min.predict(X5)
    mape = mean_absolute_percentage_error(y5, pred_full_5minmodell)
    mae = metrics.mean_absolute_error(y5, pred_full_5minmodell)
    mse = metrics.mean_squared_error(y5, pred_full_5minmodell)
    rmse = np.sqrt(mse)
    r2 = metrics.r2_score(y5, pred_full_5minmodell)
    print('Next 05 min:')
    print('MAPE:', f"{mape:.3f}", '- MAE:', f"{mae:.2f}", '- MSE:', f"{mse:.0f}", '- RMSE:', f"{rmse:.2f}", '- r2:', f"{r2:.3f}")


    # calculate metrics for 10-60 min with 2nd model
    pred_full_60minmodell = model_60min.predict(X60)
    for i in range (1,12):
        pred = pred_full_60minmodell[:,i]
        y = y60[:,i]
        mape = mean_absolute_percentage_error(y, pred)
        mae = metrics.mean_absolute_error(y, pred)
        mse = metrics.mean_squared_error(y, pred)
        rmse = np.sqrt(mse)
        r2 = metrics.r2_score(y,pred)

        print('Next', (i+1)*5,  'min:')
        print('MAPE:', f"{mape:.3f}", '- MAE:', f"{mae:.2f}", '- MSE:', f"{mse:.0f}", '- RMSE:', f"{rmse:.2f}", '- r2:', f"{r2:.3f}")
    print()    

    plt.plot(y5, c="r")
    plt.plot(pred_full_5minmodell, c="b")
    plt.xlabel('Time')
    plt.ylabel('P_WW')
    plt.legend(['Baseline', 'Prediction'], loc=2)
    plt.savefig(path+'/predict_vs_real.png')

    # plot measurements and predictions
    f, ax = plt.subplots(figsize=(15, 10))
    ax.scatter(y5, pred_full_5minmodell, c="r")
    ax.plot(pred_full_5minmodell, pred_full_5minmodell, ls="-", c="b")
    plt.legend(['Predictions', 'Measurements'], loc=4)
    plt.xlabel('TargetVariable')
    plt.ylabel('TargetVariable')
    plt.savefig(path+'/predict_vs_real_scatter.png')

    # print('prediction for next 5 min')

    # # plot measurements and predictions
    # f, ax = plt.subplots(figsize=(15, 10))
    # ax.scatter(y_test[:,0], testPredict[:,0], c="r")
    # ax.plot(testPredict[:,0], testPredict[:,0], ls="-", c="b")
    # plt.legend(['Predictions', 'Measurements'], loc=4)
    # plt.xlabel('TargetVariable')
    # plt.ylabel('TargetVariable')
    # plt.savefig('predictions/predict_5min.png')


def plot_loss(history, path):
    #plot loss and epoch
    # Get training and test loss histories
    training_loss = history['loss']
    test_loss = history['val_loss']

    # Create count of the number of epochs
    epoch_count = range(1, len(training_loss) + 1)

    plt.clf()
    # Visualize loss history
    epoch_count = range(1, len(training_loss) + 1)
    plt.plot(epoch_count, training_loss, 'r--')
    plt.plot(epoch_count, test_loss, 'b-')
    plt.legend(['Training Loss', 'Test Loss'])
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.savefig(path+'/loss_history.png')
    plt.clf()