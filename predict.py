import sys
import pickle

import numpy as np
import pandas as pd
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
from utilities import *


input_file = input("Enter path to .xlsx file: ")
if (not input_file.lower().endswith('.xlsx')):
    print("Please input a valid .xlsx file")
    exit()
    

df = pd.read_excel(input_file, header=0)
# convert timestamp strings to timestamp datatype
df['timestamp'] = pd.to_datetime(df['timestamp'], format='%Y-%m-%d %H:%M:%S')

print(f'Dataframe {input_file} successfully imported.')

# reduce features of dataframe

df = reduce_features(df)

# preprocess the input dataframe
X5, y5 = preprocess_dataset(df, steps=1)
X60, y60 = preprocess_dataset(df, steps=12)
X1h, y1h = get_1h_window(df)

# load previously generated model by main.py
reconstructed_5min_model = load_model('mlp_model1')
reconstructed_60min_model = load_model('mlp_model12')

# read history for displaying loss (this is actually history.history)
history = pickle.load(open('data/train_history12', "rb"))

# save history plot
plot_loss(history, 'predictions')

predict_and_plot(reconstructed_5min_model, reconstructed_60min_model, X5, y5, X60, y60, X1h, y1h, '/predictions')

print('Plots were saved in ./predictions')
